package com.gitee.kenewstar.multi.datasource.common;

/**
 * @author kenewstar
 */
public interface Const {

    String DEFAULT = "default";

    String MULTI_DS = "multiDataSource";

    String CONFIG_PREFIX = "spring.datasource.multi";

}
